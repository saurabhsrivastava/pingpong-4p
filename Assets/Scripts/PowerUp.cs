﻿using UnityEngine;
using System.Collections;

public abstract class PowerUp : MonoBehaviour
{
    public PowerupType type;
    public PowerupConsumibity consumibility;
    public bool isSpawned;
    public float lifetime = 3f;
    public bool isPickedup;

    protected void Start()
    {
        isSpawned = false;
        isPickedup = false;
    }

    public IEnumerator SpawnPowerup(Vector3 position)
    {
        gameObject.SetActive(true);
        gameObject.transform.position = position;

        yield return new WaitForSeconds(lifetime);

        gameObject.SetActive(false);
    }

    protected void OnPowerupAcquired()
    {
        isPickedup = false;
    }

    protected void OnPowerupConsumed()
    {
        isPickedup = true;
    }

    protected abstract void OnPickUp(PowerUp powerup, Ball ball);

    protected void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.name.Contains("Ball"))
        {
            Ball ball = collider.gameObject.GetComponent<Ball>();

            if (ball.lastHitPlayerGO != null)
            {
                ball.lastHitPlayerGO.GetComponent<PlayerController>().powerUp = this;
            }

            EventManager.OnPickedUp(this, ball);
        }
    }
}

public enum PowerupType
{ 
    FreePass,       //x
    SlowMo,         //x
    FastRelease,    //x
    Dark,
    SmallBat,       //x
    LongBat         //x
}

public enum PowerupConsumibity
{ 
    Instant,
    Delayed
}