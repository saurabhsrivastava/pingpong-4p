﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    public float speed = 2.5f;
    public GameObject lastHitPlayerGO = null;
    public bool decreaseBatSzie = false;
    private float scaleMultiplier = 0.5f;
    private float scaleLifetime = 3f;

    void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnCollision(collision);
        AssignPlayer(collision);
    }

    private void AssignPlayer(Collision2D collision)
    {
        if (collision.gameObject.name.Contains("Bat"))
        {
            lastHitPlayerGO = collision.gameObject;

            if (decreaseBatSzie)
            {
                StartCoroutine(ScaleBat(scaleMultiplier, scaleLifetime));
            }
        }
    }

    private void OnCollision(Collision2D collision)
    {
        if (collision.gameObject.name.Contains("BatLeft") || collision.gameObject.name == "Wall_Bounce_Left")
        {
            float hitFactor = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);
            Vector2 dir = new Vector2(1, hitFactor).normalized;
            gameObject.GetComponent<Rigidbody2D>().velocity = dir * speed;
        }
        else if (collision.gameObject.name.Contains("BatRight") || collision.gameObject.name == "Wall_Bounce_Right")
        {
            float hitFactor = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);
            Vector2 dir = new Vector2(-1, hitFactor).normalized;
            gameObject.GetComponent<Rigidbody2D>().velocity = dir * speed;
        }
        else if (collision.gameObject.name.Contains("BatUp") || collision.gameObject.name == "Wall_Bounce_Up")
        {
            float hitFactor = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.x, Axis.Horizontal);
            Vector2 dir = new Vector2(hitFactor, -1).normalized;
            gameObject.GetComponent<Rigidbody2D>().velocity = dir * speed;
        }
        else if (collision.gameObject.name.Contains("BatDown") || collision.gameObject.name == "Wall_Bounce_Down")
        {
            float hitFactor = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.x, Axis.Horizontal);
            Vector2 dir = new Vector2(hitFactor, 1).normalized;
            gameObject.GetComponent<Rigidbody2D>().velocity = dir * speed;
        }
        else if (collision.gameObject.name.Contains("Wall"))
        {
            int index = System.Int32.Parse(collision.gameObject.name.Split('_')[1]);
            StartCoroutine(ResetBallAndStart(index));
        }
    }

    private float HitFactor(Vector2 ballPos, Vector2 batPos, float batHeight, Axis axis = Axis.Vertical)
    {
        if (axis == Axis.Vertical)
            return (ballPos.y - batPos.y) / batHeight;

        return (ballPos.x - batPos.x) / batHeight;
    }

    private void CalculateVelocity(bool isVerticalAxis, int inactiveAxisValue, Collision2D collision)
    {
        float hitFactor = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);
        Vector2 dir = (isVerticalAxis ? new Vector2(inactiveAxisValue, hitFactor) : new Vector2(hitFactor, inactiveAxisValue)).normalized;
        gameObject.GetComponent<Rigidbody2D>().velocity = dir * speed;
    }

    public IEnumerator ResetBallAndStart(int playerIndex, float delay = 2f)
    {
        transform.position = new Vector3(0, 0, -1);
        EventManager.OnWallHit(playerIndex);
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;

        yield return new WaitForSeconds(delay);

        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    public void ManipulateBatSize(PowerupType type, float multiplier, float lifetime)
    {
        if (type == PowerupType.LongBat)
        {
            StartCoroutine(ScaleBat(multiplier, lifetime));
        }
        else
        {
            scaleMultiplier = multiplier;
            scaleLifetime = lifetime;
            decreaseBatSzie = true;
        }
    }

    private IEnumerator ScaleBat(float multiplier, float lifetime)
    {
        Vector3 lastBatSize = lastHitPlayerGO.transform.localScale;
        GameObject scaledBat = lastHitPlayerGO;
        scaledBat.transform.localScale *= multiplier;

        yield return new WaitForSeconds(lifetime);
        
        scaledBat.transform.localScale = lastBatSize;
        decreaseBatSzie = false;
    }

    private IEnumerator DecreaseBatSize()
    {
        yield return new WaitForSeconds(2f);
    }

    public enum Axis
    {
        Vertical,
        Horizontal
    }
}