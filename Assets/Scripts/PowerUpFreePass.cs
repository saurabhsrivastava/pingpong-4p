﻿using UnityEngine;
using System.Collections;

public class PowerUpFreePass : PowerUp
{
    protected void OnEnable()
    {
        EventManager._onPickedUp += OnPickUp;
    }

    protected void OnDisable()
    {
        EventManager._onPickedUp -= OnPickUp;
    }

    protected override void OnPickUp(PowerUp powerup, Ball ball)
    {
        if (powerup == this)
        {
            int index = System.Int32.Parse(ball.lastHitPlayerGO.name.Split('_')[1]);
            EventManager.OnPowerUpFreePass(index);
        }
    }
}