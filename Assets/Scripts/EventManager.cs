﻿using UnityEngine;
using System.Collections;

public class EventManager
{
    public delegate void _NoArgument();
    public delegate void _IntArgument(int value);
    public delegate void _PowerUpArgument(PowerUp value);
    public delegate void _PowerUpBallArgument(PowerUp value, Ball value2);

    public static event _IntArgument _onWallHit;
    public static event _IntArgument _onPowerUpFreePass;
    public static event _PowerUpBallArgument _onPickedUp;

    public static void OnWallHit(int value)
    {
        if (_onWallHit != null)
        {
            _onWallHit(value);
        }
    }

    public static void OnPowerUpFreePass(int value)
    {
        if (_onPowerUpFreePass != null)
        {
            _onPowerUpFreePass(value);
        }
    }

    public static void OnPickedUp(PowerUp value, Ball value2)
    {
        if (_onPickedUp != null)
        {
            _onPickedUp(value, value2);
        }
    }
}