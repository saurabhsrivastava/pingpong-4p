﻿using UnityEngine;
using System.Collections;

public class PowerupFastRelease : PowerUp
{
    public float multiplier = 2f;

    protected void OnEnable()
    {
        EventManager._onPickedUp += OnPickUp;
    }

    protected void OnDisable()
    {
        EventManager._onPickedUp -= OnPickUp;
    }

    protected override void OnPickUp(PowerUp powerup, Ball ball)
    {
        if (powerup == this)
        {
            ball.gameObject.GetComponent<Rigidbody2D>().velocity *= multiplier; 
        }
    }
}