﻿using UnityEngine;
using System.Collections;

public class DyanamicBat : PowerUp
{
    public float multiplier = 2f;

    protected void OnEnable()
    {
        EventManager._onPickedUp += OnPickUp;
    }

    protected void OnDisable()
    {
        EventManager._onPickedUp -= OnPickUp;
    }

    protected override void OnPickUp(PowerUp powerup, Ball ball)
    {
        if (powerup == this)
        {
            ball.ManipulateBatSize(type, multiplier, lifetime);
        }
    }
}