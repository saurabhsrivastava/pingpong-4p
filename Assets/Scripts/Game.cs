﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Game : MonoBehaviour
{
    public List<PlayerController> players = null;
    public Ball ball = null;
    public int noOfPlayersJoined = 4;
    public STATE gameState = STATE.None;
    private const int MAX_PLAYERS = 4;
    public GameObject playerControllerObject;
    public GameObject ballObject;
    public GameObject wallObject;

    public TextMesh textmesh;
    private Camera mainCamera;

    public List<Rect> touchAreaRect = new List<Rect>(MAX_PLAYERS);
    public List<Rect> touchAreaRectScreenCoords = new List<Rect>(MAX_PLAYERS);

    private Vector2[] batPositions = { new Vector2(-5f, 0), new Vector2(5f, 0), new Vector2(0, 8f), new Vector2(0, -8f) };
    private Vector2[] wallPositions = { new Vector2(-5.1f, 0), new Vector2(5.1f, 0), new Vector2(0, 8.1f), new Vector2(0, -8.1f) };

    public GameObject testCube;
    public GameObject[] testCubes = new GameObject[4];

    public List<Texture> bgColor = new List<Texture>();

    private PowerUp powerUpSpawned = null;
    public List<PowerUp> powerupsListGO = new List<PowerUp>();
    private List<PowerUp> powerupsList = new List<PowerUp>();
    public float spawnDuration = 5f;

    private Bounds cameraViewBounds;

    void Start()
    {
        mainCamera = Camera.main;

        gameState = STATE.GameStarted;
        InstantiatePowerUps();

        SetUpRectForTouchInput();
        StartCoroutine(StartGame());
        StartCoroutine(SpawnPowerUp(5f));
    }

    private void OnEnable()
    {
        EventManager._onWallHit += OnWallHit;
        EventManager._onPickedUp += OnPowerPickedUp;
        EventManager._onPowerUpFreePass += DecrementScore;
    }

    private void OnDisable()
    {
        EventManager._onWallHit -= OnWallHit;
        EventManager._onPickedUp -= OnPowerPickedUp;
        EventManager._onPowerUpFreePass -= DecrementScore;
    }

    public Bounds GetOrthographicBounds(Camera camera)
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        return new Bounds(camera.transform.position, new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
    }

    void Update()
    {
        //Vector3 mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        //int index = touchAreaRect.FindIndex(obj => obj.Contains(new Vector3(mousePos.x, mousePos.y)) == true);
        //textmesh.text = Ball.colliderName; // PlayerController.pos.ToString();

        //DrawRect(touchAreaRect[0], Color.blue);
        //DrawRect(touchAreaRect[1], Color.green);
        //DrawRect(touchAreaRect[2], Color.white);
        //DrawRect(touchAreaRect[3], Color.yellow);

        //textmesh.text = "" + (Vector3)PlayerController.printObj;
    }

    void OnGUI()
    {
        switch (gameState)
        {
            case STATE.WaitingForPlayers:
                noOfPlayersJoined = 4; //Input.touchCount;

                if (noOfPlayersJoined >= MAX_PLAYERS)
                {
                    noOfPlayersJoined = MAX_PLAYERS;
                }
                break;

            case STATE.GameStarted:
                //if (Event.current.type.Equals(EventType.Repaint))
                //{
                //    for (int i = 0; i < touchAreaRect.Count; i++)
                //    {
                //        GUI.DrawTexture(touchAreaRectScreenCoords[i], bgColor[i], ScaleMode.StretchToFill);
                //    }
                //}
                break;
        }
    }

    private void WaitForPlayers()
    {
    }

    private IEnumerator StartGame()
    {
        yield return new WaitForSeconds(0);

        gameState = STATE.GameStarted;

        //instantiate ball
        GameObject ballGO = Instantiate(ballObject, new Vector3(0, 0, -2), Quaternion.identity) as GameObject;
        ballGO.name = "Ball";
        ball = ballGO.GetComponent<Ball>();

        //instantiate bats
        for (int i = 0; i < noOfPlayersJoined; i++)
        {
            GameObject go = Instantiate(playerControllerObject, new Vector3(batPositions[i].x, batPositions[i].y, -1),
                (i < 2) ? Quaternion.identity : playerControllerObject.gameObject.transform.rotation) as GameObject;

            if (i < 2)
            {
                go.name = "Bat" + ((i % 2 == 0) ? "Left" : "Right") + "_" + i;
                go.GetComponent<PlayerController>().axisName = Axis.Vertical;
                go.GetComponent<PlayerController>().index = i + 1;
            }
            else
            {
                go.name = "Bat" + ((i % 2 == 0) ? "Up" : "Down") + "_" + i;
                go.GetComponent<PlayerController>().axisName = Axis.Horizontal;
                go.GetComponent<PlayerController>().index = i - 1;
            }

            //TODO : Color change
            go.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0, 256), Random.Range(0, 256), Random.Range(0, 256));
            PlayerController pc = go.GetComponent<PlayerController>();
            pc.SetLength();
            pc.touchRect = touchAreaRect[i];
            players.Add(go.GetComponent<PlayerController>());
        }

        for (int i = 0; i < 4; i++)
        {
            ///TODO: remove the Instantiattion
            GameObject go = Instantiate(wallObject, new Vector3(wallPositions[i].x, wallPositions[i].y, -1),
                (i < 2) ? Quaternion.identity : playerControllerObject.gameObject.transform.rotation) as GameObject;

            go.name = (i + 1) <= noOfPlayersJoined ? "Wall_" + i : "Wall_Bounce_" + ((i < 2) ? ((i % 2 == 0) ? "Left" : "Right") : ((i % 2 == 0) ? "Up" : "Down"));
            go.GetComponent<Wall>().axisName = (i < 2) ? Axis.Vertical : Axis.Horizontal; //"Vertical" : "Horizontal";
        }
    }

    private void SetUpRectForTouchInput()
    {
        cameraViewBounds = GetOrthographicBounds(Camera.main);

        Vector2 size = cameraViewBounds.size;
        float w = size.x;
        float h = size.y;
        float r = size.y / 4;

        float offset = 0.25f;

        batPositions[0] = new Vector3(-w / 2 + offset, 0);
        batPositions[1] = new Vector3(w / 2 - offset, 0);
        batPositions[2] = new Vector3(0, h / 2 - offset);
        batPositions[3] = new Vector3(0, -h / 2 + offset);

        offset = 0.25f;

        wallPositions[0] = new Vector3(-w / 2 - offset, 0);
        wallPositions[1] = new Vector3(w / 2 + offset, 0);
        wallPositions[2] = new Vector3(0, h / 2 + offset);
        wallPositions[3] = new Vector3(0, -h / 2 - offset);

        touchAreaRect.Add(new Rect(-w / 2, -h / 2 + r, w / 2, r * 2));
        touchAreaRect.Add(new Rect(0, -h / 2 + r, w / 2, r * 2));
        touchAreaRect.Add(new Rect(-w / 2, (h / 2) - (r * 1), w, r));
        touchAreaRect.Add(new Rect(-w / 2, -h / 2, w, r));

        for (int i = 0; i < 4; i++)
        {
            Vector3 left = mainCamera.WorldToScreenPoint(new Vector3(touchAreaRect[i].xMin, touchAreaRect[i].yMin, 0));
            Vector3 bottom = mainCamera.WorldToScreenPoint(new Vector3(touchAreaRect[i].xMax, touchAreaRect[i].yMax, 0));
            Vector3 dim = new Vector3(bottom.x - left.x, bottom.y - left.y);

            touchAreaRectScreenCoords.Add(new Rect(left.x, left.y, dim.x, dim.y));
        }
    }

    private void OnWallHit(int index)
    {
        players[index].score++;
    }

    private void DecrementScore(int index)
    {
        players[index].score--;
    }

    private void PrintScore()
    {
        textmesh.text = players[0].score + " | " + players[1].score; // + " | " + players[2].score + " | " + players[3].score;
    }

    private void InstantiatePowerUps()
    {
        foreach (PowerUp powerUp in powerupsListGO)
        {
            GameObject poerUpGO = Instantiate(powerUp.gameObject, new Vector3(0, 5000, -1), Quaternion.identity) as GameObject;
            powerupsList.Add(poerUpGO.GetComponent<PowerUp>());
        }
    }

    private PowerUp ManagePowerUp()
    {
        PowerUp powerUp = powerupsList[Random.Range(0, powerupsListGO.Count)];

        print("powerUp " + powerUp.gameObject.name);

        Vector2 size = cameraViewBounds.size;

        float w = size.x / 2;
        float h = size.y / 2;
        float x = (Random.Range(0, w * 2) - w) * 0.8f;   //rand(max - min) + min
        float y = (Random.Range(0, h * 2) - h) * 0.8f;

        powerUp.gameObject.transform.position = new Vector3(x, y, -1);
        //powerUp.gameObject.transform.position = new Vector3(0, 0, -1);
        return powerUp;
    }

    private IEnumerator SpawnPowerUp(float delayInSpawn = 0f)
    {
        yield return new WaitForSeconds(delayInSpawn);

        powerUpSpawned = ManagePowerUp();
        powerUpSpawned.isSpawned = true;

        yield return new WaitForSeconds(spawnDuration);

        if (!powerUpSpawned.isPickedup)
            DeactivatePowerUp();
    }

    private void DeactivatePowerUp()
    {
        if (powerUpSpawned == null)
            return;

        powerUpSpawned.isPickedup = false;
        powerUpSpawned.isSpawned = false;
        powerUpSpawned.gameObject.transform.position = new Vector3(0, 5000, -1);
        powerUpSpawned = null;

        StartCoroutine(SpawnPowerUp(2.5f));
    }

    private void OnPowerPickedUp(PowerUp powerUp, Ball ball)
    {
        StartCoroutine(OnPowerPickup(powerUp));
    }

    private IEnumerator OnPowerPickup(PowerUp powerUp)
    {
        if (powerUpSpawned != powerUp)
            print("hmmmm");

        powerUp.isPickedup = true;
        powerUp.gameObject.transform.position = new Vector3(0, 5000, -1f);

        yield return new WaitForSeconds(spawnDuration);

        DeactivatePowerUp();
    }

    private void DrawRect(Rect rect, Color color)
    {
        Debug.DrawLine(new Vector3(rect.x, rect.y), new Vector3(rect.x + rect.width, rect.y), color);
        Debug.DrawLine(new Vector3(rect.x + rect.width, rect.y), new Vector3(rect.x + rect.width, rect.y + rect.height), color);
        Debug.DrawLine(new Vector3(rect.x + rect.width, rect.y + rect.height), new Vector3(rect.x, rect.y + rect.height), color);
        Debug.DrawLine(new Vector3(rect.x, rect.y + rect.height), new Vector3(rect.x, rect.y), color);
    }
}

public enum STATE
{
    None,
    WaitingForPlayers,
    GameStarted,
    Pause,
    GameOver
}

public enum Axis
{
    Vertical,
    Horizontal
}