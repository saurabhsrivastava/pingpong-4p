﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public SpriteRenderer sprite;
    public Axis axisName = Axis.Horizontal;
    public float speed = 15f;
    public int index = -1;
    public Rect touchRect;
    public int score = 0;
    public PowerUp powerUp;
    public static object printObj;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    public static Vector3 pos = Vector3.zero;

    void Update()
    {
#if UNITY_EDITOR
        float v = Input.GetAxisRaw(axisName.ToString() + index.ToString());
        gameObject.GetComponent<Rigidbody2D>().velocity = speed * (axisName == Axis.Horizontal ? new Vector2(v, 0) : new Vector2(0, v));
#else
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Moved && touchRect.Contains(Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position)))
                {
                    //Vector3 touchDeltaPosition = Input.GetTouch(i).deltaPosition;
                    //if (touchRect.Contains(Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position)))
                    //{
                    //    gameObject.GetComponent<Rigidbody2D>().velocity = speed * (axisName == Axis.Horizontal ? new Vector2((touchDeltaPosition.x > 0) ? 1 : -1, 0) : new Vector2(0, (touchDeltaPosition.y > 0) ? 1 : -1)).normalized;
                    //}

                    Vector3 inputPos = Input.GetTouch(i).deltaPosition;
                    if (axisName == Axis.Horizontal)
                    {
                        Vector3 touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(0, inputPos.y, -1));
                        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, new Vector3(inputPos.x > 0 ? -touchPosition.x : touchPosition.x, gameObject.transform.position.y, -1), Time.deltaTime * speed);
                    }
                    else
                    {
                        Vector3 touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(inputPos.x, 0, -1));
                        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, new Vector3(gameObject.transform.position.x, inputPos.y > 0 ? -touchPosition.y : touchPosition.y, -1), Time.deltaTime * speed);
                    }
                }
            }
        }
#endif
    }

    public void SetLength()
    {
        float width = Screen.width;
        float height = Screen.height;

        float ratio = width / height;

        Vector3 scale = gameObject.transform.localScale;

        if (axisName == Axis.Vertical)
            scale.x *= ratio;
        else
            scale.y *= ratio;

        gameObject.transform.localScale = scale;
    }
}